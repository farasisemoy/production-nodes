#!/bin/bash
# Need to create dedicated users for slurm since the default unix accounts have been associated with LDAP. 
# Unsure if forcing those LDAP accounts to have matching uid/gid will cause issues, so creating dedicated unix/slurm users just to be safe

FEU=("wjiang" "asarhadian" "hfu" "ssamdani" "ssong" "sjangra" "wxie" "wzhou" "xliu" "yzhang" "emoy")
FEE=("ahenry" "jfath" "phermann" "rmousavi" "wwangschw")

SIM=(${FEU[@]} ${FEE[@]})

COUNTER=1100
for i in "${SIM[@]}"
do
  let COUNTER++ 
  xid="$COUNTER" 
  sui_name="sss_$i"

  echo "XID: $xid, ID: $sui_name"
  sudo groupadd -r -g "$xid" "$sui_name" \
  && sudo useradd -r -u "$xid" -g "$xid" -m -c "scriptserver-slurm user account" -d /home/"$sui_name" -s /bin/bash $sui_name
  sudo usermod -a -G scriptserver "sss_$i" 

  sudo mkdir -p /srv/slurm/"$sui_name"
  sudo chown scriptserver /srv/slurm/"$sui_name"
  sudo chgrp scriptserver /srv/slurm/"$sui_name"
  sudo chmod 775 /srv/slurm/"$sui_name"
done
