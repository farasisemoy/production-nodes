# manual mount of intel-sim04's scriptsrever uploadFiles directory
# 'user_allow_other' must be uncommented in /etc/fuse.conf for intel-sim01/02/03
# TODO: change to auto-mount
sudo su scriptserver
sshfs scriptserver@intel-sim04:/opt/script-server/temp/uploadFiles /mnt/opt/script-server/temp/uploadFiles -C -o idmap=user -o allow_other
