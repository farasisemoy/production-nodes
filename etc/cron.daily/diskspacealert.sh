#!/bin/bash

THRESHOLD=75
BACKSTOP=90

function check_diskspace() {
  usep=$(echo $output | awk '{ print $1}' | cut -d'%' -f1 )
  partition=$(echo $output | awk '{print $2}' )
  subject="$(hostname) disk space status"
  if [[ $usep -ge $THRESHOLD ]];
  then
    if [[ $usep -ge $BACKSTOP ]];
    then
      subject="Warning: $(hostname) is $usep% full as of $(date)."
      msg="${subject} Please transfer files and lower disk usage immediately to avoid system issues."
      echo $msg
      echo "----------------------------"
      echo `du -shc /home/* /samba/shares/* | sort -h -r | head -n 6 | tail -5`
    else
      subject="Caution: $(hostname) is $usep% full as of $(date)."
      msg="${subject} Please transfer files and lower disk usage immediately to avoid system issues."
      echo $msg
      echo "----------------------------"
      echo `du -shc /home/* /samba/shares/* | sort -h -r | head -n 6 | tail -5`
    fi
    /usr/sbin/ssmtp -t <<EOT
      To: EN/SM - Global Simulation & Advanced Modeling <Farasis_GlobalSim@farasis.com>
      From: simulation-cluster@farasis.com
      Subject: $subject

      Hi everyone,

      $msg

      Current top usage on `hostname`
      ----------------------------
      `du -shc /home/* /samba/shares/* | sort -h -r | head -n 6 | tail -5`

      Thank you,
      Farasis Simulation Cluster
EOT
# the second EOT cannot be indented at all, otherwise there will be an error!
  fi

}

if [[ $(hostname) == 'intel-sim01' ]]; # this is due to there being a separate /home virtual partition on intel-sim01 only
then
  df -PkH | grep -vE '^Filesystem|tmpfs|cdrom|media|/dev/nvme0n1p1|/dev/nvme0n1p2|scriptserver@intel-sim04:/opt/script-server/temp/uploadFiles|/dev/mapper/centos-root' | awk '{ print $5 " " $6 }' | while read output;
  do
    check_diskspace
  done
else
  df -PkH | grep -vE '^Filesystem|tmpfs|cdrom|media|/dev/nvme0n1p1|/dev/nvme0n1p2|scriptserver@intel-sim04:/opt/script-server/temp/uploadFiles' | awk '{ print $5 " " $6 }' | while read output;
  do
    check_diskspace
  done
fi
