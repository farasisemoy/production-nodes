alias ll="ls -al"

MODULESHOME=/etc/modulefiles

### ESI-Group begin ###
#
#       ESI-Group Software environment
#
#PAMHOME=/tools/VPS/2019r0u1
PAMHOME=/tools/VPS/VPSSolver
PAMENV=$PAMHOME/env-`uname`
export PAMHOME PAMENV
if [ -r $PAMENV/psi.Baenv ]; then
    . $PAMENV/psi.Baenv
fi
# Next line to avoid error
BASH_ENV=${BASH_ENV:-""}
if [ -z "$BASH_ENV" ];then
    BASH_ENV=$HOME/.bashrc
    export BASH_ENV
fi
#PAM_LMD_LICENSE_FILE=27000@FEI-FS6
PAM_LMD_LICENSE_FILE=28000@FEI-PDM.farasis.net
export PAM_LMD_LICENSE_FILE
### ESI-Group end ###
alias vps20="pamcrash -dp -mpi impi -mpidir $PAMHOME/intelmpi/2018.3/Linux_x86_64/intel64/bin/ $1"
alias vps19="pamcrash19 -dp -mpi impi -mpidir $PAMHOME/intelmpi/2018.3/Linux_x86_64/intel64/bin/ $1"
alias vps18="pamcrash18 -dp -mpi -mpidir $PAMHOME/pcmpi/Linux_x86_64/9.01.04.03/bin/ -mpiext '-TCP' $1"

### LS-DYNA BEGIN ###

# LS-RUN
alias lsrun21="/ansys_inc/v212/ansys/bin/./lsrun"

alias vps20sp="pamcrash -mpi impi -mpidir $PAMHOME/intelmpi/2018.3/Linux_x86_64/intel64/bin/ $1"
### LS-DYNA END ###