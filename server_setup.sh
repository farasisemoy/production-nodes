############################################################################## 
# assuming following static IPs
# sim01: 10.10.0.106
# sim02: 10.10.0.104
# sim03: 10.10.0.107
# sim04: 10.10.0.105
##############################################################################

##############################################################################
############# user account setup #############
##############################################################################

# setting UIDs consistantly across all nodes

##############################################################################
############# sudo access setup #############
##############################################################################

##############################################################################
############# slurm scheduler setup #############
##############################################################################

# sshfs

# slurm-mail

##############################################################################
############# scriptserver UI setup #############
# git@github.com:bugy/script-server.git
##############################################################################


# scriptserver user account setup

##############################################################################
############# shared uploads mount (from script-server setup) #############
##############################################################################

##############################################################################
############# shared uploads mount (from script-server setup) #############
## Mounting /opt/script-server/temp/uploads to /mnt/opt/script-server/temp/uploads on each server
##############################################################################

# for sim01, sim02, sim03, run sshfs once to mount sim04's /opt/script-server/temp/uploadFiles directory
scriptserver@10.10.0.105:/opt/script-server/temp/uploadFiles /mnt/opt/script-server/temp/uploadFiles/ fuse.sshfs noauto,x-systemd.automount,_netdev,users,idmap=user,IdentityFile=/var/lib/script-server/.ssh/id_ed25519,allow_other,reconnect 0 0

# on sim04 only (since we don't actually need to mount, only create symbolic link)
sudo mkdir -p /mnt/opt/script-server/temp
sudo chown -R /mnt/opt scriptserver:scriptserver
sudo chmod -R 775 /mnt/opt
sudo ln -s /opt/script-server/temp/uploadFiles /mnt/opt/script-server/temp/uploadFiles


##############################################################################
############# crontab setup #############
##############################################################################



##############################################################################
############# environment-modules #############
##############################################################################

# Perform this command on all sim nodes
sudo yum install -y environment-modules

# LS-DYNA specific
# on all sim nodes, create ls-dyna and module version file 1.0 in /usr/share/Modules/modulefiles/
sudo mkdir -p /usr/share/Modules/modulefiles/ls-dyna

# use whatever is the latest version of this modulefile 
sudo cat > /usr/share/Modules/modulefiles/ls-dyna/1.0 << EOF
#%Module

proc ModulesHelp { } {
   puts stderr "This module sets PATH and LD_LIBRARY_PATH for Ansys LS-DYNA jobs"
}
 
module-whatis "This module sets PATH and LD_LIBRARY_PATH for Ansys LS-DYNA jobs\n"

setenv  I_MPI_PMI2          1
setenv  I_MPI_PMI_LIBRARY   /usr/lib64/libpmi2.so
setenv  LSTC_LICENSE        ANSYS
setenv  I_MPI_DEBUG         5

set basedir "/usr/ansys_inc/v212"
prepend-path PATH "${basedir}/commonfiles/MPI/Intel/2018.3.222/linx64/bin"
prepend-path LD_LIBRARY_PATH "${basedir}/commonfiles/MPI/Intel/2018.3.222/linx64/lib"
prepend-path LD_LIBRARY_PATH "${basedir}/commonfiles/fluids/lib/linx64"

EOF

##############################################################################
############# Manual slurm submission #############
## Short-term work-around for no high-speed shared filespace on sim01, sim02, sim03
##############################################################################

# on sim01, sim02, and sim03, run the following commands to mount sim04's scriptserver upload directory to the other nodes
sudo chmod -R 775 /mnt/opt 
sudo chmod +x /usr/local/bin/slurm/script/remount_sshfs.sh
/usr/local/bin/slurm/script/remount_sshfs.sh 


##############################################################################
############# Web portal setup #############
##############################################################################


##############################################################################
############# Emails #############
##############################################################################
sudo yum install ssmtp