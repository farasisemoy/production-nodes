<div id="layout">
    <!-- Menu toggle -->
    <a href="#menu" id="menuLink" class="menu-link">
        <!-- Hamburger icon -->
        <span></span>
    </a>
    <div id="menu">
        <div class="pure-menu">
            <a class="pure-menu-heading" href="http://10.10.0.105/">Sim Cluster</a>
            <ul class="pure-menu-list">
                <li class="pure-menu-item"><a href="http://10.10.0.105/" class="pure-menu-link">Job queue</a></li>
                <li class="pure-menu-item"><a href="http://10.10.0.105/ui.php" class="pure-menu-link">Job submit</a></li>
                <li class="pure-menu-item"><a href="https://farasis.atlassian.net/wiki/spaces/SIM/pages/2032631947/Linux+HPC+intel-sim+User+Guide" class="pure-menu-link" target="_blank">User manual</a></li>
            </ul>
        </div>
    </div>
</div>