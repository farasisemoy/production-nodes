<!DOCTYPE html>
    <?php include "common/header.php" ?>
        <meta http-equiv="refresh" content="60">
    </head>
    <body>
        <?php include "common/sidebar.php" ?>

        <div id="main">
            <div class="header">
                <h1>Intel-Sim Queue</h1>
                <p>(queue updates every minute)</p>
            </div>

            <div class="content">
                <h2 class="content-subhead">Queue information</h2>
                <?php include "_queue.html" ?>
            </div>
        </div>
        
    </body>
</html>