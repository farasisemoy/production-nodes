#!/bin/bash
# path: /usr/ansys_inc/v212/ansys/bin/linx64/man_slurm_lsdyna_sp.sh
# wrapper script for LS_DYNA SMP_SINGLE_PRECISION executible, called from LS-DYNA GUI (/ansys_inc/v212/ansys/bin/lsrun)

while getopts i:n:m: flag
do
    case "${flag}" in
        i) input=${OPTARG};;
        n) ncpu=${OPTARG};;
        m) memory=${OPTARG};;
    esac
done

# Set memory to auto if not specified
if [ -z "$memory" ]; then 
    memory=auto
else
    memory="$memory"
fi

USER=`whoami`
SLURM_USER=sss_"$USER"
MAIL_USER="$USER"@farasis.com                                                # emoy@farasis.com
PARTITION=medium
SCRIPT_TYPE='Manual slurm job (LS-DYNA: SMP-SP)'
CORES="$ncpu"
USER_WORKING_DIR=`dirname "$input"`

# Parse time to accepted format
LIMIT_TIME=24:0:0

# Set job name
TIMESTAMP=`date -u +"%FT%H%MZ"`
FILENAME=$(basename -- "$input")                                              # 1.pc
JOB_NAME="$FILENAME"_MANUAL_"$TIMESTAMP"

# Get account
FEU=("wjiang" "asarhadian" "samdani" "ssong" "wxie" "kfung" "wzhou" "xliu" "yzhang" "emoy" "pding")
FEE=("ahenry" "jfath" "phermann" "rmousavi" "wwangschw" "czhang")

if containsElement "$USER" "${FEU[@]}"; then
    ACCOUNT='feusim'
elif containsElement "$USER" "${FEE[@]}"; then
    ACCOUNT='feesim'
else
    ACCOUNT='feusim'
fi

echo "Input: $input"
echo "NCPU: $ncpu"
echo "Memory: $memory"
echo "User: $USER"
echo "Slurm User: $SLURM_USER"
echo "Working DIR: $USER_WORKING_DIR"
echo "Mail User: $MAIL_USER"
echo "Partition: $PARTITION"
echo "Script type: $SCRIPT_TYPE"
echo "CORES: $CORES"
echo "Limit time: $LIMIT_TIME"
echo "Timestamp: $TIMESTAMP"
echo "FILENAME: $FILENAME"
echo "JOBNAME: $JOB_NAME"
echo "Account: $ACCOUNT"

COMMENT="$SCRIPT_TYPE" : "$ncpu" cores : "$memory" memory

# Slurm batch submission
sbatch <<EOT
#!/bin/bash

# Specify slurm parameters
#SBATCH -o "$OUT_FILE"
#SBATCH -e "$ERR_FILE"
#SBATCH --begin=now
#SBATCH --mail-type=ALL
#SBATCH --mail-user="$USER"
#SBATCH --account="$ACCOUNT"
#SBATCH --partition="$PARTITION"
#SBATCH --chdir="$USER_WORKING_DIR"
#SBATCH --job-name="$JOB_NAME"
#SBATCH --comment="$COMMENT"
#SBATCH --cpus-per-task="$ncpu"
#SBATCH --time="$LIMIT_TIME"

# Clear the environment from any previously loaded modules
module purge > /dev/null 2>&1

# Submit job to 
echo "sending LS-DYNA SMP_Single_Precision job to Slurm (manual/LS-DYNA GUI triggered)" > "$OUT_FILE"
srun /usr/local/bin/slurm/script/run_lsdyna_sp.sh  -i "$INPUT_WORKING_FILEPATH" -n "$ncpu" -m "$memory"

EOT
