#!/bin/bash
# wrapper script for LS_DYNA SMP_DOUBLE_PRECISION executible, called from LS-DYNA GUI (/ansys_inc/v212/ansys/bin/lsrun)

while getopts i:n:m flag
do
    case "${flag}" in
        i) input=${OPTARG};;
        n) ncpu=${OPTARG};;
        m) memory=${OPTARG};;
    esac
done

USER=`whoami`

/usr/ansys_inc/v212/ansys/bin/linx64/lsdyna_dp.e i=$input ncpu=$ncpu memory=$memory