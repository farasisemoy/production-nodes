#!/bin/bash

######################################
# Info: This script is meant to serve as both Scriptserver-UI and 'manual' (linux terminal) scriptserver interface
# 
# Instructions: 
# 1) Copy this script to your home directory working folder where you normally run your scripts. 
# 2) Modify the input parameters in section "MANUAL / TERMINAL SCRIPTSERVER VARIABLES TO SET"
# 
# Note: Currently needs to be run as root / superuser unfortunately in order to set the correct slurm user account
######################################

# function for getting ceiling integer
ceildiv(){ echo $((($1+$2-1)/$2)); }

# command line arguments (optional)
while getopts "S:u:p:c:i:f:n:t:d:w:m:s:F:" flag;
do
    case "${flag}" in
        S) SCRIPT_TYPE=${OPTARG};;        # (default) ansys_ls_dyna
        u) SCRIPTSERVER_USER=${OPTARG};;  # (default) emoy
        p) PARTITION=${OPTARG};;          # (specify) partition
        c) CORES=${OPTARG};;              # (specify) cores
        i) UPLOAD_FILEPATH=${OPTARG};;    # (specify) filepath
        f) MAIN_FILENAME=${OPTARG};;      # (specify) main filename in zip
        n) ORIG_JOB_NAME=${OPTARG};;      # (specify) job name
        t) EST_TIME=${OPTARG};;           # (specify) estimated time
        d) CFX_DIRECT_CCL_FILEPATH=${OPTARG};; # (ansys CFX specific)
        w) ANSYS_WORKSPACE_FACTOR=${OPTARG};;  # (ansys CFX specific)
        m) NODES_PARAM=${OPTARG};;        # (specify) ls-dyna number of nodes
        s) SOLVER_TYPE=${OPTARG};;        # (specify) ls-dyna solver type (e.g. MPP_Single_Precision or MPP_Double_Precision)
    esac
done

################################# MANUAL / TERMINAL SCRIPTSERVER VARIABLES TO SET #################
############### Only edit variables in this block for default scriptserver behavior ###############

SCRIPT_TYPE=ansys_ls_dyna                         # set SCRIPT_TYPE (ansys_ls_dyna, ansys_cfx, or ansys_mech)
PARTITION=sim01debug                                  # set PARTITION (high, medium, low, sim01debug, sim02debug, sim03debug, sim04debug)
CORES=32                                          # set CORES (max 32)
EST_TIME=1                                        # set EST_TIME (in hours, max 24)
ORIG_JOB_NAME=emoy01_manual_job                  # set JOB NAME (to uniquely identify job)
# set input name (input file should be in same directory as this script)
INITIAL_INPUT_NAME=combined_mpp_7b0.k

SCRIPTSERVER_USER="emoy"                         # set this to be the base username (without 'sss_' prefix)


#### LS-DYNA only ####
NODES_PARAM=100000                               # set NODES_PARAM, the number of nodes for LS-DYNA job (minimum 50000, maximum 2500000)
SOLVER_TYPE=MPP_Single_Precision                 # set LS-DYNA solver type (MPP_Single_Precision, MPP_Double_Precision)
# MAIN_FILENAME=                                 # specify the main filename to pass to LS-DYNA if using zip file as input  


#### CFX only ####
# CFX_DIRECT_CCL_FILEPATH=                         # set optional CCL_FILEPATH
# ANSYS_WORKSPACE_FACTOR=2                         # set ANSYS_WORKSPACE_FACTOR

############################## Scriptserver terminal mode setup ###################################

# set SCRIPTSERVER_USER (also for SLURM_USER and MAIL_USER) variables.. currently doesn't work due to root privileges needed to run this script from terminal
SUB='sss_'
# if [[ "$USER" == *"$SUB"* ]]; then
#   SSS_USER=(${USER//_/ })
#   SCRIPTSERVER_USER=${SSS_USER[1]}
# else
#   SCRIPTSERVER_USER="$USER"
# fi


# copy upload file to correct shared directory
if [[ "$HOSTNAME" == "intel-sim04" ]]; then
  echo "intel-sim04"
  SCRIPTSERVER_UPLOADS_DIR=/opt/script-server/temp/uploadFiles/script_uploads
  cp "/home/sss_emoy/src2"/"$INITIAL_INPUT_NAME" /opt/script-server/temp/uploadFiles/script_uploads
else
  echo "not intel-sim04"
  SCRIPTSERVER_UPLOADS_DIR=/mnt/opt/script-server/temp/uploadFiles/script_uploads
  cp "/home/sss_emoy/src2"/"$INITIAL_INPUT_NAME" /mnt/opt/script-server/temp/uploadFiles/script_uploads
fi

UPLOAD_FILEPATH="$SCRIPTSERVER_UPLOADS_DIR"/"$INITIAL_INPUT_NAME"

############ Default scriptserver-UI script below (shared with manual script as well) ############

SLURM_USER=sss_"$SCRIPTSERVER_USER"                                                 # sss_emoy
MAIL_USER="$SCRIPTSERVER_USER"@farasis.com                                          # emoy@farasis.com
USER_WORKING_DIR=/srv/slurm/"$SLURM_USER"                                           # general output directory for slurm user (e.g. /srv/slurm/sss_emoy/)

LOCAL_FILEPATH=$(basename -- "$UPLOAD_FILEPATH")                                    # 1.pc
FILE_EXTENSION="${LOCAL_FILEPATH#*.}"
RAND_STRING=`cat /dev/urandom | tr -cd 'a-f0-9' | head -c 3`                        # to ensure unique job descriptor
INPUT_FILENAME="${LOCAL_FILEPATH%%.*}"_"$RAND_STRING"                               # 1_as2.pc
INPUT_WORKING_FILEPATH="$USER_WORKING_DIR"/"$INPUT_FILENAME"."$FILE_EXTENSION"      # /srv/slurm/sss_emoy/1_as2.pc

OUT_FILE="$USER_WORKING_DIR"/"$INPUT_FILENAME".out                                  # /srv/slurm/sss_emoy/1_as2.out
ERR_FILE="$USER_WORKING_DIR"/"$INPUT_FILENAME".err                                  # /srv/slurm/sss_emoy/1_as2.err

TIMESTAMP=`date -u +"%FT%H%MZ"`

containsElement () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

# Get account
FEU=("wjiang" "asarhadian" "ssamdani" "wxie" "wzhou" "xliu" "emoy" "pding")
FEE=("ahenry" "jfath" "phermann" "wwangschw" "czhang")

if containsElement "$SCRIPTSERVER_USER" "${FEU[@]}"; then
    ACCOUNT='feusim'
elif containsElement "$SCRIPTSERVER_USER" "${FEE[@]}"; then
    ACCOUNT='feesim'
else
    ACCOUNT='other'
fi

# Determine default behavior for each script type
if [[ $SCRIPT_TYPE == *"ansys_ls_dyna"* ]]; then
    if [ -z "$PARTITION" ]; then PARTITION='medium'; fi
elif [[ $SCRIPT_TYPE == *"ansys_cfx"* ]]; then
    if [ -z "$PARTITION" ]; then PARTITION='medium'; fi
    CORES="${CORES//[!0-9]/}"

    CCL_LOCAL_FILEPATH=$(basename -- "$CFX_DIRECT_CCL_FILEPATH")                                    # /opt/script-server/temp/uploads/test.def
    CCL_FILE_EXTENSION="${CCL_LOCAL_FILEPATH#*.}"
    CCL_LOCAL_FILENAME="${CCL_LOCAL_FILEPATH%%.*}"_"$RAND_STRING"                                   # test_as2
    CCL_WORKING_FILEPATH="$USER_WORKING_DIR"/"$CCL_LOCAL_FILENAME"."$CCL_FILE_EXTENSION"            # /srv/slurm/sss_emoy/test_as2.def
fi

# Set job name as filename if not previously provided
if [ -z "$ORIG_JOB_NAME" ]; then 
    JOB_NAME="$INPUT_FILENAME"_"$RAND_STRING"
    CLEAN_JOB_NAME="$INPUT_FILENAME"
else
    JOB_NAME="$ORIG_JOB_NAME"_"$RAND_STRING"
    CLEAN_JOB_NAME="$ORIG_JOB_NAME"
fi

# ANSYS LS-DYNA nodes to memory conversion
MEMORY_PARAM=20m
if [ -z "$NODES_PARAM" ]; then 
    MEMORY_PARAM=20m    # default 20m
else
    MEMORY_PARAM=`ceildiv "$NODES_PARAM"*4 10000`m      # otherwise convert nodes to m memory
fi

# Define final output directory (for copying to storage after job completition)
FINAL_OUTPUT_DIR="$USER_WORKING_DIR"/_"$CLEAN_JOB_NAME"_"$TIMESTAMP"

# Parse time to accepted format
EST_TIME="$EST_TIME":0:0

# Output to scriptserver console
echo "Partition: $PARTITION"
echo "Num Cores: $CORES"
echo "Script type: $SCRIPT_TYPE"
echo "Output log file: $OUT_FILE"
echo "Error file: $ERR_FILE"
echo "Working directory: $USER_WORKING_DIR"
echo "Final output directory: $FINAL_OUTPUT_DIR"
echo "Defined job name: $JOB_NAME"

# Debug only
echo "Slurm user: $SLURM_USER"
echo "Upload temp filepath: $UPLOAD_FILEPATH"
echo "Local filepath (upload.def): $LOCAL_FILEPATH"
echo "Input filename w/ random string ('upload_a93', w/ no .ext): $INPUT_FILENAME"
echo "Input working filepath: $INPUT_WORKING_FILEPATH"
echo "MAIN FILENAME: $MAIN_FILENAME"
echo "Error file: $ERR_FILE"
echo "Solver Type: $SOLVER_TYPE"
echo "Nodes Param: $NODES_PARAM"
echo "Memory Param: $MEMORY_PARAM"
echo "Minimum time specified: $EST_TIME"
echo "File Extension: $FILE_EXTENSION"

# Slurm batch submission
sbatch <<EOT
#!/bin/bash

# Specify slurm parameters
#SBATCH --partition="$PARTITION"
#SBATCH -N 1
#SBATCH --ntasks="$CORES"
#SBATCH --cpus-per-task=1
#SBATCH --time="$EST_TIME"
#SBATCH --uid="$SLURM_USER"
#SBATCH --account="$ACCOUNT"
#SBATCH -o "$OUT_FILE"
#SBATCH -e "$ERR_FILE"
#SBATCH --mail-type=ALL
#SBATCH --mail-user="$MAIL_USER"
#SBATCH --chdir="$USER_WORKING_DIR"
#SBATCH --job-name="$CLEAN_JOB_NAME"
#SBATCH --comment="$SCRIPT_TYPE"
#SBATCH --wait

###
# Pre-job actions
###

# Set HOME VAR (need to validate if this works..)
export HOME=/srv/slurm/"$SLURM_USER"

# Source modules
source /etc/profile.d/modules.sh

# Clear the environment from any previously loaded modules
module purge > /dev/null 2>&1

# Create specific output transfer directory (but we won't move files there til after job completition)
# e.g. /srv/slurm/sss_emoy/_test30_2022-01-25T2132Z
mkdir "$USER_WORKING_DIR"/_"$CLEAN_JOB_NAME"_"$TIMESTAMP"

# Copy uploads to working directory
# e.g. from /mnt/opt/script-server/temp/uploadFiles/b28bcb8bab55/1643146352708/cellswelling.zip to /srv/slurm/sss_emoy/cell_swelling_as2.zip
cp "$UPLOAD_FILEPATH" "$INPUT_WORKING_FILEPATH"

# Do different tasks depending on script type
if [[ $SCRIPT_TYPE == *"ansys_cfx"* ]]; then
  # For Ansys CFX, copy CCL file if present
  cp "$CFX_DIRECT_CCL_FILEPATH" "$CCL_WORKING_FILEPATH"
elif [[ $SCRIPT_TYPE == *"ansys_ls_dyna"* ]]; then
  if [[ "$FILE_EXTENSION" == "zip" ]]; then
    # Unzip uploaded includes file zip to working directory if necessary
    if file "$INPUT_WORKING_FILEPATH" | grep -iq zip; then 
      unzip -o -qq "$INPUT_WORKING_FILEPATH" -d "$USER_WORKING_DIR"
    fi 
  fi
  
else
  touch "$USER_WORKING_DIR"/failure.err 
  echo "Failure for input file: "
  echo "$USER_WORKING_DIR"/"$MAIN_FILENAME" >> "$USER_WORKING_DIR"/failure.err
fi

# (can remove below line) Rename input file (e.g for vps19 from /srv/slurm/sss_emoy/1.pc to /srv/slurm/sss_emoy/1_as2.pc)
# mv "$FINAL_OUTPUT_DIR"/"$LOCAL_FILEPATH" "$FINAL_OUTPUT_DIR"/"${MAIN_FILENAME%%.*}"_"$RAND_STRING"

###
# Run jobs now
###
case $SCRIPT_TYPE in
  "ansys_cfx")
    CCL_FILE=$INPUT_WORKING_FILEPATH
    CCL_FILE=$CCL_WORKING_FILEPATH
    if [[ -z "$CFX_DIRECT_CCL_FILEPATH" ]]; then
        srun /usr/local/bin/slurm/script/run_ansys_cfx.sh -D "$INPUT_WORKING_FILEPATH" -c "$CORES" -w "$ANSYS_WORKSPACE_FACTOR" -o "$OUT_FILE"
    else
        srun /usr/local/bin/slurm/script/run_ansys_cfx.sh -D "$INPUT_WORKING_FILEPATH" -c "$CORES" -w "$ANSYS_WORKSPACE_FACTOR" -o "$OUT_FILE" -C "$CCL_WORKING_FILEPATH"
    fi
    ;;
  "ansys_ls_dyna")
    module load ls-dyna/1.0

    case "$SOLVER_TYPE" in
    MPP_Single_Precision)
        echo "sending LS-DYNA MPP_Single_Precision job to Slurm" >> "$OUT_FILE"
        if [[ -z "$MAIN_FILENAME" ]]; then
          srun --mpi=pmi2 --cpu_bind=verbose,cores /usr/ansys_inc/v212/ansys/bin/linx64/lsdyna_sp_mpp.e i="$INPUT_WORKING_FILEPATH" o="$OUT_FILE" e="$ERR_FILE"
        else
          srun --mpi=pmi2 --cpu_bind=verbose,cores /usr/ansys_inc/v212/ansys/bin/linx64/lsdyna_sp_mpp.e i="$USER_WORKING_DIR"/"$MAIN_FILENAME" o="$OUT_FILE" e="$ERR_FILE"
        fi
        ;;
        
    MPP_Double_Precision)
        echo "sending LS-DYNA MPP_Double_Precision job to Slurm" >> "$OUT_FILE"
        if [[ -z "$MAIN_FILENAME" ]]; then
          srun --mpi=pmi2 --cpu_bind=verbose,cores /usr/ansys_inc/v212/ansys/bin/linx64/lsdyna_dp_mpp.e i="$INPUT_WORKING_FILEPATH" o="$OUT_FILE" e="$ERR_FILE"
        else
          srun --mpi=pmi2 --cpu_bind=verbose,cores /usr/ansys_inc/v212/ansys/bin/linx64/lsdyna_dp_mpp.e i="$USER_WORKING_DIR"/"$MAIN_FILENAME" o="$OUT_FILE" e="$ERR_FILE"
        fi
        ;;
    *)
        echo "Invalid LS-DYNA solver type" > "$OUT_FILE"
        ;;
    esac
esac

### 
# Post-job actions
###

# unload all modules
module purge

# Move job files to output directory
# For LS-DYNA, move all files in base working folder /srv/slurm/<sss_user> that are not 'scp_status.txt' to the final export folder
if [[ $SCRIPT_TYPE == *"ansys_ls_dyna"* ]]; then
  # find "$USER_WORKING_DIR" -maxdepth 1 -mindepth 1 -type f -not -name scp_status.txt -print0 | xargs -0 sudo mv -t "$USER_WORKING_DIR"/_"$CLEAN_JOB_NAME"_"$TIMESTAMP"
  cd "$USER_WORKING_DIR"
  
  # Clean up messsage output files
  cat mes0* > combined_message.txt
  rm mes0*

  mv adptmp bg_switch binout* *.err *.out cont_profile* d3* kill_by_pid* load_profile* combined_message.txt *.k status.out "$USER_WORKING_DIR"/_"$CLEAN_JOB_NAME"_"$TIMESTAMP"
fi

if [ -z "$MAIN_FILENAME" ]; then
  mv "$USER_WORKING_DIR"/"$MAIN_FILENAME" "$USER_WORKING_DIR"/_"$CLEAN_JOB_NAME"_"$TIMESTAMP"
fi
mv "$USER_WORKING_DIR"/"$INPUT_FILENAME".* "$USER_WORKING_DIR"/_"$CLEAN_JOB_NAME"_"$TIMESTAMP"

# Remove rand suffix from filenames of job files
# for f in "$USER_WORKING_DIR"/_"$CLEAN_JOB_NAME"_"$TIMESTAMP"/*
# do
#     REMOVE_SUFFIX=_"$RAND_STRING"
#     FILE_EXTENSION="${f##*.}"           ### store extension in a variable.
#     f="${f%.*}"                         ### remove extension
#     f="${f%"$REMOVE_SUFFIX"}"           ### remove suffix
#     f="$f.$FILE_EXTENSION"              ### re-place the extension
# done

# Move output directory to simulation storage drive on HPE Windows Server (D:/intel-sim-jobs/[username]/)
sshpass -f "/srv/slurm/.hpe_pass" scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r "$USER_WORKING_DIR"/_"$CLEAN_JOB_NAME"_"$TIMESTAMP" scriptserver@10.10.0.34:/D:/intel_sim_jobs/"$SCRIPTSERVER_USER"

# Targeted cleanup after copying job working directory to simulation storage drive
if [ $? -eq 0 ];
then
    touch "$USER_WORKING_DIR"/scp_status.txt
    echo "SCP transfer OK for "$USER_WORKING_DIR"/_"$CLEAN_JOB_NAME"_"$TIMESTAMP": 10.10.0.34:/D:/intel_sim_jobs/$SCRIPTSERVER_USER" >> "$USER_WORKING_DIR"/scp_status.txt
    # rm -rf "$USER_WORKING_DIR"/_"$CLEAN_JOB_NAME"_"$TIMESTAMP"/ # commented out by default
else
    touch "$USER_WORKING_DIR"/scp_status.txt
    echo "SCP transfer ERROR for "$USER_WORKING_DIR"/_"$CLEAN_JOB_NAME"_"$TIMESTAMP": 10.10.0.34:/D:/intel_sim_jobs/$SCRIPTSERVER_USER" >> "$USER_WORKING_DIR"/scp_status.txt
fi

exit 0
EOT
