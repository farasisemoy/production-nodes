#!/usr/bin/env python

import os
import subprocess
import numpy as np
import pandas as pd

def generate_table(multiline_input, border=1, show=0):
    # nodes = ['sim01', 'sim02', 'sim03', 'sim04']
    # busy_cores = [0, 0, 0, 0]
    # free_cores = [108, 108, 108, 108]

    MAX_CORES = 108

    cluster_cores_used = {
        "intel-sim01": 0,
        "intel-sim02": 0,
        "intel-sim03": 0,
        "intel-sim04": 0    
    }

    def header_sanitize(i): # sanitize default squeue titles
        switcher={
                'PARTITION':'PRIORITY',
                'NODELIST':'NODE',
                'CPUS':'CORES',
                'COMMENT':'TYPE',
                'TIME':'RUNTIME',
                'END_TIME':'LIMIT'
             }
        return switcher.get(i,i)

    html = f"<table class='pure-table pure-table-bordered' style='table-layout:fixed'>"
    for num, line in enumerate(multiline_input.strip().splitlines(), start=1):
        if num==1 :
            html += "<tr>"
            for item in line.split():
                if item=='NAME' or item=='END_TIME':
                    html += f"<th style='width:100%'>{header_sanitize(item)}</th>"
                else:
                    html += f"<th>{header_sanitize(item)}</th>"
            html += "</tr>" 
        else :
            test_list = line.split()
            html += "<tr>"
            print(f'test_list: {test_list}')
            for idx in range(0, len(test_list)):
                print(f'idx: {idx}')
                if idx==4 and test_list[7]=="RUNNING" :                   
                    cluster_cores_used[test_list[idx-1]] += int(test_list[idx])
                cell_value=test_list[idx]
                if idx==2 : # sanitize username
                    cell_value = cell_value.replace('sss_', '')
                if idx==3 : # shorten nodename
                    cell_value = cell_value.replace('intel-', '')
                # if idx==6 : # sanitize jobname from format 'name_hash' to 'name (hash)'
                    #uniqhash = test_list[idx][-3:]
                    #cell_value = cell_value.replace(test_list[idx][-4:], f" ({uniqhash})")
                if idx==9 : # shorten nodename
                    cell_value = cell_value.replace('T', ' ')
                html += f"<td class='table-cell' style='word-wrap: break-word'>{cell_value}</td>"
            html += "</tr>"
    html += "</table>"    

    free_cores = {
        "intel-sim01": MAX_CORES - cluster_cores_used["intel-sim01"],
        "intel-sim02": MAX_CORES - cluster_cores_used["intel-sim02"],
        "intel-sim03": MAX_CORES - cluster_cores_used["intel-sim03"],
        "intel-sim04": MAX_CORES - cluster_cores_used["intel-sim04"]
    }

    html += f"<br>"
    html += f"<h2 class='content-subhead'>Cores available</h2>"
    html += f"<table class='pure-table pure-table-bordered'><tr><th>Node</th><th>Cores</th></tr>"
    for node in free_cores:
        fc = free_cores[node]
        html += "<tr><td>%s</td><td>%s</td></tr>" % (node, fc)
    html += f"</table>"
    if show == 1:
        # filename = "/usr/local/var/www/queue.html"        # local testing
        filename = "/var/www/html/_queue.html"              # server path
        with open(filename, "w") as file:
            file.write(html)
    return html

subprocess.run(["/usr/local/bin/slurm/exec_squeue.sh"])
file = open("/var/www/html/squeue.txt", "r")
lines = file.read()
file.close()

t = generate_table(lines, show=1)
print(t)
    