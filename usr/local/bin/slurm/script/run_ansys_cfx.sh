#!/bin/bash

# called by slurm submission script to run ansys CFX_Direct
# 1) .def file path
# 2) # of cores 
# 3) user specified parameter for workspace factor
# 4) optional .ccl file

# Set HOME variable
export HOME=/srv/slurm

# This segment is working
while getopts "D:C:c:w:o:" flag;
do
    case "${flag}" in
        D) DEF_FILE=${OPTARG};;
        C) CCL_FILE=${OPTARG};;
        c) CORE_COUNT=${OPTARG};;
        w) WORKSPACE_FACTOR=${OPTARG};;
        o) OUT_FILE=${OPTARG};;
    esac
done

# Replace hostname and # of cores in the .ccl input file with sed
HOSTNAME=`hostname`
CONDENSED_HOSTNAME="${HOSTNAME//-}"
sed -i "s/.*Number of Processes.*/\tNumber of Processes = $CORE_COUNT/g" $DEF_FILE
sed -i "s/.*Parallel Host List.*/\tParallel Host List = $CONDENSED_HOSTNAME/g" $DEF_FILE

if [[ -z "$CCL_FILE" ]]; then
  echo "NO CCL FILE" >> $OUT_FILE
  /usr/ansys_inc/v211/CFX/bin/cfx5solve -def $DEF_FILE -parallel -part $CORE_COUNT -double -large -size $WORKSPACE_FACTOR -start-method "Platform MPI Local Parallel" >> $OUT_FILE
  # /usr/ansys_inc/v211/CFX/bin/cfx5solve -def $DEF_FILE -ccl $CCL_FILE -parallel -part $CORE_COUNT -double -large -size $WORKSPACE_FACTOR -start-method "Platform MPI Local Parallel" >> $OUT_FILE
else
  echo "WITH CCL FILE" >> $OUT_FILE
  /usr/ansys_inc/v211/CFX/bin/cfx5solve -def $DEF_FILE -ccl $CCL_FILE -parallel -part $CORE_COUNT -double -large -size $WORKSPACE_FACTOR -start-method "Platform MPI Local Parallel" >> $OUT_FILE
fi

# original (working)
# /usr/ansys_inc/v211/CFX/bin/cfx5solve -def test.def -ccl test.ccl -parallel -part 36 -double -large -size 2 -start-method "Platform MPI Local Parallel"