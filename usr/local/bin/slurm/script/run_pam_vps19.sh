#!/bin/bash
# called by slurm submission script to run vps19

PAMHOME=/tools/VPS/VPSSolver
PAMENV=$PAMHOME/env-`uname`
export PAMHOME PAMENV
if [ -r $PAMENV/psi.Baenv ]; then
    . $PAMENV/psi.Baenv
fi

PAM_LMD_LICENSE_FILE=28000@FEI-PDM.farasis.net
export PAM_LMD_LICENSE_FILE

$PAMHOME/pamcrash_safe/2019.0/Linux_x86_64/bin/pamcrash -sp -dp -np $2 -mpi impi -mpidir $PAMHOME/intelmpi/2018.3/Linux_x86_64/intel64/bin/ $1 >> $3