#!/bin/bash

# called by slurm submission script to run ansys mechanical (v211)
# 1) .input file path
# 2) # of cores 

# Set HOME variable
export HOME=/srv/slurm

# This segment is working
while getopts "i:c:o:" flag;
do
    case "${flag}" in
        i) INPUT_FILE=${OPTARG};;
        c) CORE_COUNT=${OPTARG};;
        o) OUTPUT_FILE=${OPTARG};;
    esac
done

/usr/ansys_inc/v211/ansys/bin/ansys211 -b -dis -mpi intelmpi -np $CORE_COUNT -i $INPUT_FILE -o $OUTPUT_FILE

# original (working)
# /usr/ansys_inc/v211/ansys/bin/ansys211 -b -dis -mpi intelmpi -np 96 -i NBRnew.dat -o NBRnew.out