#!/bin/bash
# Remounts sshfs directory on intel-sim04 to this other intel-sim server (sim01, sim02, or sim03)

sshfs scriptserver@intel-sim04:/opt/script-server/temp/uploadFiles /mnt/opt/script-server/temp/uploadFiles -C -o idmap=user -o allow_other