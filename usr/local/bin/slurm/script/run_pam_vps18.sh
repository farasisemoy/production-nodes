#!/bin/bash
# called by slurm submission script to run vps18

PAMHOME=/tools/VPS/VPSSolver
PAMENV=$PAMHOME/env-`uname`
export PAMHOME PAMENV
if [ -r $PAMENV/psi.Baenv ]; then
    . $PAMENV/psi.Baenv
fi
# Next line to avoid error
BASH_ENV=${BASH_ENV:-""}
if [ -z "$BASH_ENV" ];then
    BASH_ENV=$HOME/.bashrc
    export BASH_ENV
fi
PAM_LMD_LICENSE_FILE=28000@FEI-PDM.farasis.net
export PAM_LMD_LICENSE_FILE

$PAMHOME/pamcrash_safe/2018.0/Linux_x86_64/bin/pamcrash -sp -dp -np $2 -mpi -mpidir $PAMHOME/pcmpi/Linux_x86_64/9.01.04.03/bin/ -mpiext '-TCP' $1 >> $3