#!/bin/bash

while getopts "s:i:n" flag;
do
    case "${flag}" in
        s) seconds=${OPTARG};;
        i) iterations=${OPTARG};;
        n) non=${OPTARG};; # dummy flag option
    esac
done

squeue -o "%.7i %.8P %.12N %.4C %.20j %.16u %.2t %.8M %.6D" -S N > /var/www/html/squeue.txt
