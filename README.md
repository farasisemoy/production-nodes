# Simulation Cluster README


## Hardware
| Hostname | IP address | Operating System | Notes | 
| --- | ---| --- | --- |
| intel-sim01 | 10.10.0.106 | Linux (CentOS 7) | Worker node|
| intel-sim02 | 10.10.0.104 | Linux (CentOS 7) | Worker node |
| intel-sim03 | 10.10.0.107 | Linux (CentOS 7) | Worker node |
| intel-sim04 | 10.10.0.105 | Linux (CentOS 7) | Central management, accouting, worker node|
| FEI-SIM-VISUAL-001 | 10.10.0.34 | Windows HPE Server 2019 | Storage node |

<br><br>

## Software

### General purpose / utility
```
# for loading different sets of environment variables / setup depending on the program being used, slurm-mail
sudo yum install environment-modules ssmtp
```

### Slurm (https://slurm.schedmd.com)
v20.11.5 (requires upgrade due to security issue)

Linux based scheduler comprised of three parts
* slurmctld (central management daemon on intel-sim04)
* slurmdbd (accounting database daemon on intel-sim04)
* slurmd (compute node daemon on intel-sim01,02,03,04)
<br>

### script-server (https://github.com/bugy/script-server)
v1.16.0

Web-UI interface hosted on intel-sim04 (http://10.10.0.103:51555/index.html)
* Web-UI users managed via htpasswd utility
* Normal sim team users only need to access cluster via this interface, no actual linux terminal access required
<br>


<br><br>

## Accounts Overview

All users have and will access these accounts
1. [System-level] Windows ActiveDirectory account (used to access storage drive @ 10.10.0.34). This is managed by ActiveDirectory
2. [Application-level]Script-server account (used to access web UI). This is managed by .htpasswd on intel-sim04

All users have but will NOT directly access these accounts, thus no passwords needed to be provided for them
3. [Application-level] Slurm account (used for slurmdbd accounting). This account is tied to #4 (unix account 'sss_*') via uid. Managed by sacctmgr utility on intel-sim04
4. [System-level] Special Unix accounts (prefixed by 'sss_'). Managed by unix admins on all intel-sim nodes 01-04

<br><br>

## User Workflow
Pre-requisites: User must be connected to FEU-VPN
1. (intel-sim04, web-ui) Using script-server web UI, submit job type, parameters, and input files (http://10.10.0.105:51555/index.html)
2. (intel-sim04) Input files and parameters uploaded to intel-sim04, then slurm submission script is executed
3. (intel-sim04) Slurm job is created, queued, and assigned to worker (intel-sim0X) based on resource availability
4. (intel-sim0X) Worker node accesses uploaded files from intel-sim04 via sshfs and executes slurm job using given parameters
5. (windows) Output copied from worker node to D:/intel_sim_jobs directory on the HPE Windows Server (10.10.0.34)
6. (intel-sim04) Email notification to user from intel-sim04 once the job has completed

<br><br>

## Linux system file structure

File structure of intel-sim04 (main slurm controller node)
<pre>
|-- /etc/slurm                  # Slurm scheduler 
|   |-- slurm.conf              # Slurm configuration file
|   |-- slurmdbd.conf           # Slurm database daemon configuration file
|
|-- /mnt/opt/script-server      # Mount point for intel-sim01/02/03 
|
|-- /opt/script-server          # Scriptserver web-app (http://10.10.0.105:51555) running on intel-sim04
|   |--.htpasswd                # Scriptserver web-app user authentication (use this file to add/modify/delete users for web-app)
|   |-- conf
|   |   |-- configuration.json  # Scriptserver web-app configuration file
|   |-- temp
|       |-- uploads             # Scriptserver job submission uploads are placed here. 
|                               # The uploads directory on intel-sim04 is mounted via sshfs on intel-sim01/02/03 @ /mnt/opt/script-server
|
|-- /srv/slurm                  # Slurm service working directory. Each slurm user has working directory prefixed with 'sss_' (e.g. /srv/slurm/sss_ssong)
|   |-- .ansys                  # Needs to be present for ansys scripts to be working, permissions set to 777
|   |-- user_folder             # Each slurm job's output is located here prior to automated HPE server transfer via scp
|
|-- /usr/local/bin/slurm
|   |-- script
|   |   |-- run_pam_vps19.sh    # VPS19 solver script called by slurm submission
|   |-- submit.slurm            # Slurm submission script called by scriptserver web-app
|
|-- /var/lib/scriptserver       # scriptserver user home directory
|   |-- .hpe_pass               # contains password for ssh/scp access to HPE server (10.10.0.34), used in file transfer from /srv/slurm
|
|-- /var/log/slurm              # Slurm controller and daemon log files 
|   |-- slurmd.log              
|   |-- slurmctld.log
|   |-- slurmdbd.log
|
|-- /var/www/html               # Slurm queue UI 
</pre>
<br><br>

## Monitoring Interface
(https://www.suse.com/c/monitoring-sle-hpc-prometheus-grafana/)

* Install Go on slurmctld server (intel-sim04)
* Install [prometheus-slurm-exporter](https://github.com/vpenso/prometheus-slurm-exporter). Then run server and validate at 10.10.0.103:8080/metrics
* Install [prometheus](https://geekflare.com/prometheus-grafana-setup-for-linux/)
* Install influx-db

```
sudo systemctl restart prometheus.service -l
sudo systemctl restart grafana-server
sudo systemctl status prometheus.service -l
sudo systemctl status grafana-server
```

<br><br>
## Setup guide
Install
* slurm
* script-server (git@github.com:bugy/script-server.git). Then see 'Source code tweaks' section
* sshfs

## Loading modules
```
# List current loaded modules
module list

# Show available modules to load
module avail

# Load module
module load [module-name]

# Unload module
module unload [module-name]

# Add new directory for modules
module use --append [directory-path]
```

<br><br>
## Source code tweaks
On intel-sim04 where script-server is running..
```
### Modified one flag in the python Tornado webserver (/opt/script-server/src/web/server.py) to allow UI embedding within queue iframe
See https://github.com/bugy/script-server/issues/77

### Old: 
self.set_header('X-Frame-Options', 'DENY')
### New: 
self.set_header('X-Frame-Options', 'ALLOW')
```

<br><br>
## Crontabs

```
# slurm user
# sudo su slurm | crontab -e
* * * * * python3 /opt/slurm-mail/bin/slurm-send-mail.py
```

```
# root user (for each node sim01-04)
# sudo crontab -e
* * * * * python3 /usr/local/bin/slurm/parse_squeue.py >/dev/null 2>&1
```

```
# crontabs that run outside of (sudo) crontab -e
# /etc/cron.*

# 1) /etc/cron.daily/diskspacealert.sh  # this sends an email alert to sim team once a node is past 75%/90% threshold
```

<br><br>
## TODO (Eugene)
1. Provide Aria list of htpasswd user/password combos for script-server UI
2. Make admin command guide for administrating htpasswd


## Additional config

### SSMTP
```
# sudo yum install ssmtp
# configure /etc/ssmtp/ssmtp.conf with the following (to enable emailing from cronjob)
root=simulation-cluster@farasis.com
mailhub=smtp.office365.com:587
UseSTARTTLS=YES
AuthUser=simulation-cluster@farasis.com
AuthPass= # Ask Aria / IT for password to this account
RewriteDomain=farasis.com
FromLineOverride=YES
```