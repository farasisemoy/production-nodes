# Queue

* crontab every minute outputs results of `squeue` to `/var/www/html/squeue.txt'
* parse squeue.txt data into html partials via python script
* calculate current free cores and cores available when next job finishes for each node

* create vuejs page from partials
* integrate vuejs page into scriptserver interface